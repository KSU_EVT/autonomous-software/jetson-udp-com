import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

import socket
import numpy as np
import cv2
import struct

class ImageReceiverNode(Node):
    def __init__(self):
        super().__init__('image_receiver_node')

        self.bridge = CvBridge()

        # Listen on all available network interfaces
        # self.host = self.declare_parameter('~host', '192.168.0.120').value
        self.host = self.declare_parameter('~host', '10.0.0.2').value
        self.port = self.declare_parameter('~port', 5342).value
        self.max_datagram_size = 2**16

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.host, self.port))
        self.dump_buffer()

        self.timer = self.create_timer(0.01, self.image_callback)
        self.pub = self.create_publisher(Image, '/camera/track_seg', 10)

    def dump_buffer(self):
        """ Emptying buffer frame """
        while True:
            seg, addr = self.sock.recvfrom(self.max_datagram_size)
            print(seg[0])
            if struct.unpack("B", seg[0:1])[0] == 1:
                print("finish emptying buffer")
                break
    
    def postProcess(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        _, blackWhite = cv2.threshold(img, 128, 256, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

        contour, _ = cv2.findContours(blackWhite,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contour:
            cv2.drawContours(blackWhite, [cnt], -1, (255, 255, 255), -1)

        contour, _ = cv2.findContours(blackWhite,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
        biggest = max(contour, key=cv2.contourArea)

        copy = cv2.cvtColor(blackWhite, cv2.COLOR_GRAY2RGB)

        cv2.drawContours(copy, [biggest], -1, (0, 0, 235), -1)

        black_pixels = np.where(
            (copy[:, :, 0] == 255) &
            (copy[:, :, 1] == 255) &
            (copy[:, :, 2] == 255) 
        )

        copy[black_pixels] = [0, 0, 0]

        return(cv2.GaussianBlur(copy, (5, 5), 1))

    def image_callback(self):
        dat = b''

        while True:
            seg, addr = self.sock.recvfrom(self.max_datagram_size)
            if struct.unpack("B", seg[0:1])[0] > 1:
                dat += seg[1:]
            else:
                dat += seg[1:]
                img = cv2.imdecode(np.fromstring(dat, dtype=np.uint8), 1)
                msg = self.bridge.cv2_to_imgmsg(self.postProcess(img), 'rgb8')
                self.pub.publish(msg)
                dat = b''


def main(args=None):
    rclpy.init(args=args)
    node = ImageReceiverNode()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
